import numpy as np
import numpy.random as rnd
from scipy.optimize import linprog

from joblib import Memory
location = './cachedir'
memory = Memory(location, verbose=0)

def P_construct(nb_state):
    P = np.zeros((nb_state, nb_state))
    for row_nb in range(nb_state):
        Y = rnd.exponential(size=nb_state)
        P[row_nb, :] = Y/sum(Y)
    return P

def R_construct(nb_state):
    return rnd.uniform(0, 1, nb_state)

def PR_construct(nb_state, seed = None):
    if seed is not None: rnd.seed(seed)

    return P_construct(nb_state), P_construct(nb_state),  R_construct(nb_state),  R_construct(nb_state)

def transition(P, state):
    return np.argmax(rnd.multinomial(1, P[state, :]))

def bandit_to_rho(bandits, S):
    return np.array([sum(bandits == s) for s in S])/bandits.shape[0]

def lp_solver(A, S, T_max, rho, P, R, alpha):
    T = np.array(range(1, T_max+1))
    total_len = A.shape[0] * S.shape[0] * T.shape[0]

    # rewards
    reward = np.array([0. for _ in range(total_len)])
    cpt = 0
    for a in A:
        for s in S:
            for t in T:
                reward[cpt] = -R[a][s]   #-r(s, a)
                cpt += 1

    # budget condition
    budget = np.ones(T_max) * alpha

    mat_budget = np.zeros((T_max, total_len))
    for t in T:
        for i, s in enumerate(S):
            mat_budget[t-1, S.shape[0]*T.shape[0] + t-1 + i*T.shape[0]] = 1


    # initial occupation
    init_occupation = rho.copy()
    
    mat_init_occupation = np.zeros((S.shape[0], total_len))
    for i, s in enumerate(S):
        for j, a in enumerate(A):
            mat_init_occupation[i, i*T_max + S.shape[0]*T_max*j] = 1

    #flow balance
    flow_balance = np.zeros((S.shape[0]*(T_max-1)))
    mat_flow_balance = np.zeros((S.shape[0]*(T_max-1),total_len))

    for i, s in enumerate(S):
        for t in T[1:]:
            for j, a in enumerate(A):
                mat_flow_balance[t-2 + i*(T_max-1), i*T_max + S.shape[0]*T_max*j + t-1] = 1
                for k, s2 in enumerate(S):
                    mat_flow_balance[t-2 + i*(T_max-1), k*T_max + S.shape[0]*T_max*j + t-2] = -P[a][s2, s]
                    
            
    # concatenating the conditions
    Aeq = np.concatenate((mat_budget, mat_init_occupation, mat_flow_balance), axis = 0)
    beq = np.concatenate((budget, init_occupation, flow_balance))
    
    rho_star = linprog(reward, A_eq = Aeq, b_eq = beq, bounds=(0, None))
    print('rho is', rho_star.x, 'and sums to', sum(rho_star.x))
    return rho_star.x, rho_star.fun
lp_solver_joblib = memory.cache(lp_solver)


def determine_action(t, policy, bandits, N, alpha):
    action_t = np.zeros(N)
    left_to_activate = int(alpha*N)

    for state in np.argsort(-policy[t]):             
        for i, bandit in enumerate(bandits):
            if left_to_activate <= 0:
                break
            if bandit == state:
                action_t[i] = 1
                left_to_activate -= 1

    return action_t



def print_debug(t, policy, bandits, action_t):
    print("t :", t)
    print("policy", policy)
    print("Policy recommends : ", np.argsort(-policy))
    print("Bandits are in states : ", bandits)
    print("Bandits number ", action_t, "are selected")
    print("="*50)
    
    
def experiment(nb_state, alpha, N, T_max, P, R, debug=False, step_resolution=1, bandits=None, seed=1, joblib_id=0):
    A = np.array([0, 1])
    P0, P1 = P
    R0, R1 = R
    S = np.array(range(nb_state))
    policy_1 = np.zeros((T_max, S.shape[0]))
    values = np.zeros(T_max)
    
    
    if bandits is None:
        bandits = np.zeros(N, dtype = int)
    
    t = 0
    while t < T_max:
        rho = bandit_to_rho(bandits, S)
        
        T_left = T_max - t
        
        if joblib_id == 0:
            solv, value_bound = lp_solver_joblib(A, S, T_left, rho, [P0, P1], [R0, R1], alpha)
        else:
            solv, value_bound = lp_solver(A, S, T_left, rho, [P0, P1], [R0, R1], alpha)

        if T_left == T_max: bound = -value_bound

        for step in range(min(step_resolution, T_left)):
            # policy - probability to take action a = 1
            policy_1[t] = np.array([solv[1 * S.shape[0]*T_left + s * T_left + step] 
                                        / sum([solv[a2 * S.shape[0]*T_left + s * T_left + step] for a2 in A]) 
                                      for s in S])    

            # arms to activate
            action_t = determine_action(t, policy_1, bandits, N, alpha)

            #debugging 
            if debug:
                print_debug(t, policy_1[t], bandits, action_t)

            # action is taken, an observation is made
            
            values[t] = sum([R1[s] if action_t[i] else R0[s] for i,s in enumerate(bandits)])
            #print('actions',action_t)
            #print('values ', values)
            bandits = np.array([transition(P1, s) if action_t[i] else transition(P0, s) for i, s in enumerate(bandits)])

            t += 1
    print(values)
    return values, policy_1, bandits, bound
