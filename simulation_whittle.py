"""
This module contains code for doing simulation for Whittle's Index Policy, nb_of_states could be greater than 3.

"""

import numpy as np
from qft import *
import random
import joblib
from joblib import Parallel, delayed
import time

LOCATION = './cachedir'
MEMORY = joblib.Memory(LOCATION, verbose=0) # to cache results

def simulate_one_bandit_one_step(proba_line):
    n = len(proba_line)
    seed = random.uniform(0,1)
    position = 0
    while sum(proba_line[0:position+1]) < seed:
        position += 1
    return position

def simulate_one_step(states, M, P0, P1):
    i, a = actives2(states, M)
    n = len(states)
    next_states = np.zeros(n, dtype = int)
    for j in range(i):
        proba_line = P1[j]
        for _ in range(states[j]):
            p = simulate_one_bandit_one_step(proba_line)
            next_states[p] += 1
    for _ in range(a):
        proba_line = P1[i]
        p = simulate_one_bandit_one_step(proba_line)
        next_states[p] += 1
    for _ in range(states[i]-a):
        proba_line = P0[i]
        p = simulate_one_bandit_one_step(proba_line)
        next_states[p] += 1
    for j in range(i+1,n):
        proba_line = P0[j]
        for _ in range(states[j]):
            p = simulate_one_bandit_one_step(proba_line)
            next_states[p] += 1
    return next_states

def reward_one_step(states, M, R1):
    i, a = actives2(states,M)
    print("actives2=",states, i, a)
    return(np.dot(states[0:i],R1[0:i]) + a*R1[i])

"""
Random initial states for each simulation

"""

def start_state(N, n):
    l = np.zeros(n, dtype=int)
    l[0] = N
    return l

def simulate(N, M, P0, P1, R1, time_horizon):
    print(N,M)
    n = len(R1)
    reward = 0
    states = start_state(N, n)
    print('time horizon', time_horizon)
    for _ in range(time_horizon):
        reward += reward_one_step(states, M, R1)
        print('reward ', reward/N,  'expected=',R1[0]*0.3)
        states = simulate_one_step(states, M, P0, P1)
    return reward/(N*time_horizon)

@MEMORY.cache
def simulate_with_confidence(N, M, P0, P1, R1, repeat_times = 9600, time_horizon = 10000):
    start = time.time()
    simulate(N,M,P0,P1,R1, time_horizon)
    reward_vector = Parallel(n_jobs=-1,batch_size=200)(delayed(simulate)(N,M,P0,P1,R1, time_horizon) for _ in range(repeat_times))
    #print('time to do simulations = {:1.2f} s'.format(time.time()-start)) 
    r = np.mean(reward_vector)
    ci = 2 * np.std(reward_vector)/np.sqrt(repeat_times-1)
    return r,ci

