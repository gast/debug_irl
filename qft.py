import numpy as np
import matplotlib.pyplot as plt
import random
import copy
from matplotlib.pyplot import figure
from scipy.spatial import ConvexHull
from scipy.stats import multinomial


def transmatrix(n):
    P = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            P[i,j] = random.uniform(0,1)
    for i in range(n):
        s = np.sum(P[i])
        for j in range(n):
            P[i,j] = P[i,j]/s
    return P


def steady_state_prop(transition_matrix):
    
    dim = transition_matrix.shape[0]
    q = (transition_matrix-np.eye(dim))
    ones = np.ones(dim)
    q = np.c_[q, ones]
    QTQ = np.dot(q, q.T)
    bQT = np.ones(dim)
    return np.linalg.solve(QTQ, bQT)


def give_paras(n):
    P0 = transmatrix(n)
    P1 = transmatrix(n)
    R1 = [random.uniform(0,1) for i in range(n)]
    return P0,P1,R1


def give_matrix(i,P0,P1):
    '''
    The program will return the transformation matrix when the threshold happens at state i
    
    '''
    P = np.copy(P0)
    for j in range(i):
        P[j] = P1[j] - P1[i] + P0[i]
    return P

def init_condition(N,n):
    s = np.zeros(n)
    for i in range(n):
        s[i] = random.uniform(0,1)
    total = np.sum(s)
    for i in range(n):
        s[i] = int(N*s[i]/total)
    s[n-1] = N - sum(s[0:n-1])
    return s   

def actives1(m, alpha):
    """
    return (i, a) where:
    - states 0 to i-1 are actives
    - state i is active with probability "a"
    
    """
    s = 0
    i = 0
    n = len(m)
    while i < n and s+m[i] <= alpha:
        s += m[i]
        i += 1
    if s == alpha:
        return i,0
    else:
        return i, (alpha-s)/m[i]

def actives2(s,M):
    """
    return (i, a) where:
    - states 0 to i-1 are actives
    - state i is active with number of bandits a
    
    """
    N = np.sum(s)
    total = 0
    i = 0
    while i < N and total+s[i] < M:
        total += s[i]
        i += 1
    return i, M-total


def sort_matrices_by_indices(P0,P1,R1):
    indices = get_Whittle(P0,P1,R1)
    if indices == 0:
        print('Oooops not indexable!!!')
    else:
        indices.reverse()
        n = len(indices)
        new_P0 = np.zeros((n,n))
        new_P1 = np.zeros((n,n))
        new_R1 = np.zeros((n))
        for i in range(n):
            for j in range(n):
                new_P0[i,j] = P0[indices[i][0], indices[j][0]]
                new_P1[i,j] = P1[indices[i][0], indices[j][0]]
            new_R1[i] = R1[indices[i][0]]
        return new_P0, new_P1, new_R1
    
def subset1(n):
    nums = [i for i in range(n)]
    subsets = [[]]
    for i in nums:
        prev = copy.deepcopy(subsets)
        [k.append(i) for k in subsets]
        subsets.extend(prev) 
    return subsets

def subset2(n):
    nums = [i for i in range(n)]
    subsets = [[]]
    for i in nums:
        prev = copy.deepcopy(subsets)
        [k.append(i) for k in subsets]
        subsets.extend(prev) 
    for i in range(len(subsets)):
        subsets[i] = [subsets[i],i]
    return subsets

def steady(S,P0,P1,R1):
    l=len(S)
    P = copy.deepcopy(P0)
    f = 0.
    g = 0.
    if l == 0:
        return 0,0
    else:
        for i in S:
            P[i] = P1[i]
        pi = steady_state_prop(P)
        for i in range(l):
            f += pi[S[i]]*R1[S[i]]
        for i in range(l):
            g += pi[S[i]]
        return f,g  
    
def split(u, v, points):
    # return points on left side of UV
    return [p for p in points if np.cross(np.subtract(p[0], u[0]), np.subtract(v[0],u[0])) < 0]

def extend(u, v, points):
    if not points:
        return []

    # find furthest point W, and split search to WV, UW
    w = min(points, key=lambda p: np.cross(np.subtract(p[0], u[0]), np.subtract(v[0], u[0])))
    p1, p2 = split(w, v, points), split(u, w, points)
    return extend(w, v, p1) + [w] + extend(u, w, p2)

def convex_hull(points):
    # find two hull points, U, V, and split to left and right search
    u = min(points, key=lambda p: p[0][0])
    v = max(points, key=lambda p: p[0][0])
    left, right = split(u, v, points), split(v, u, points)
    
    # find convex hull on each side
    return [v] + extend(u, v, left) + [u] + extend(v, u, right) 

def get_Whittle(P0,P1,R1):
    n = np.shape(P0)[0]
    S = subset2(n)
    N = 2**n
    points = [[[0.,0.],0]]*N
    
    for i in range(N-1):
        x = steady(S[i][0],P0,P1,R1)[1]
        y = steady(S[i][0],P0,P1,R1)[0]
        points[i] = [[x, y],i]
    points[N-1] = [[0.,0.],N-1]
    hull = convex_hull(points)
    
    ind = 0
    while hull[ind][1] != N-1:
        ind +=1
    
    if ind != n:
        return 0
    else:
        mylist = [S[hull[i][1]][0] for i in range(ind)]
        mylist.append([])
        Whittle = []
        for i in range(ind):
            Whittle.append(list(set(mylist[i]).difference(set(mylist[i+1]))))
        if len(Whittle) != n:
            return 0
        else:
            test = 0
            while len(set(Whittle[test])) == 1 and len(mylist[test]) == n-test and test < (n-1):
                test += 1
            if test != n-1:
                return 0
            else:
                return Whittle


def onestep_deterministic_model(P0,P1,m,alpha):
    """
    We assume that P0, P1 are sorted by indices
    """
    transition_matrix = np.copy(P0)
    i, a = actives1(m, alpha)
    for j in range(i):
        transition_matrix[j] = P1[j]
    transition_matrix[i] = a*P1[i] + (1.-a)*P0[i]
    return np.tensordot(m, transition_matrix,1)

def simulate_iterates(P0, P1, m, alpha, T = 1000):
    for t in range(T):
        m = onestep_deterministic_model(P0,P1,m,alpha)
        #print(m)
    return m

def simulate_from_random_initial_condition(P0, P1, alpha,T = 1000):
    n = np.shape(P0)[0]
    m = np.random.rand((n))
    m /= np.sum(m)
    return simulate_iterates(P0, P1, m, alpha, T)

def change_to_int(m):
    return [int(i) for i in np.multiply(m,10000)]

def test_unique_fixed_point(P0, P1, R1, alpha, number_of_initial_values=500,T = 1000):
    '''
    Test of indexability beforehand
    
    '''
    if get_Whittle(P0,P1,R1) != 0:
        
        P0, P1, R1 = sort_matrices_by_indices(P0,P1,R1)
        fixed_point = simulate_from_random_initial_condition(P0, P1, alpha)
        fixed_point_set = [fixed_point]
        for i in range(number_of_initial_values):
            m = simulate_from_random_initial_condition(P0, P1, alpha,T)
            m_int = change_to_int(m)
            fix_point_set_int = [change_to_int(i) for i in fixed_point_set]
            indicator = 0
            for j in range(len(fixed_point_set)):
                if set(m_int) == set(fix_point_set_int[j]):
                    break
                indicator += 1
            if indicator == len(fixed_point_set):
                fixed_point_set.append(m)
        return fixed_point_set
    else:
        print('Ooooops not indexable!!!')

def prop_of_active(P0,P1,prob,s):
    transition_matrix = np.copy(P0)
    for j in range(s):
        transition_matrix[j] = P1[j]
    transition_matrix[s] = prob*P1[s] + (1-prob)*P0[s]
    steady = steady_state_prop(transition_matrix)
    if s > 0:
        active = np.sum(steady[0:s]) + steady[s]*prob
    else: 
        active = steady[0]*prob
    return active

def find_state(P0,P1,alpha):
    '''
    We assume that P0, P1 are sorted by indices,
    the program will return the state where threshold happens
    
    '''
    n = np.shape(P0)[0]
    s = np.zeros(n)
    i = 0
    active = 0.
    while active < alpha:
        transition_matrix = np.copy(P0)
        i += 1
        for j in range(i):
            transition_matrix[j] = P1[j]
        m = steady_state_prop(transition_matrix)
        active = np.sum(m[0:i])
    return i-1

def find_fixed_point_by_direct_calculation(P0,P1,alpha):
    n = np.shape(P0)[0]
    s = find_state(P0,P1,alpha)
    prob_up = 1
    prob_down = 0
    tol = 1e-8
    err = prop_of_active(P0,P1,(prob_up + prob_down)*0.5,s) - alpha
    while abs(err) > tol:
        if err < 0:
            prob_down = (prob_up + prob_down)*0.5
        else:
            prob_up = (prob_up + prob_down)*0.5
        err = prop_of_active(P0,P1,(prob_up + prob_down)*0.5,s) - alpha
    prob = (prob_up + prob_down)*0.5
    transition_matrix = np.copy(P0)
    for j in range(s):
        transition_matrix[j] = P1[j]
    transition_matrix[s] = prob*P1[s] + (1-prob)*P0[s]
    steady = steady_state_prop(transition_matrix)
    return steady 


def give_index(P0,P1,R1):
    '''
    We assume that the paras passes the indexability test and the states are already sorted!!!!!!
    
    '''
    n = np.shape(P0)[0]
    S = subset2(n)
    N = 2**n
    points = [[[0.,0.],0]]*N
    
    for i in range(N-1):
        x = steady(S[i][0],P0,P1,R1)[1]
        y = steady(S[i][0],P0,P1,R1)[0]
        points[i] = [[x, y],i]
    points[N-1] = [[0.,0.],N-1]
    hull = convex_hull(points)
    mylist = [S[hull[i][1]][1] for i in range(n)]
    mylist.append(N-1)
    index = [0.]*n
        
    for j in range(n):
            
        coordinate2 = points[mylist[j+1]][0]         
        coordinate1 = points[mylist[j]][0]
        index[j] = (coordinate1[1] - coordinate2[1])/(coordinate1[0] - coordinate2[0])
    
    index.reverse()
    return index


def onetrialplot(P0,P1,R1):
    '''
    Plot the convex hull of a given paras with number of states n
    
    '''
    n = len(R1)
    S = subset1(n)
    N = 2**n
    points = np.zeros((N,2))
    x = np.zeros(N)
    y = np.zeros(N)
    
    for i in range(N-1):
        x[i] = steady(S[i],P0,P1,R1)[1]
        y[i] = steady(S[i],P0,P1,R1)[0]
        points[i] = [x[i], y[i]]
    points[N-1] = [0.,0.]
    figure(num=None, figsize=(15, 9), dpi=80, facecolor='w', edgecolor='k')
    fig, ax = plt.subplots()
    for i, txt in enumerate(S):
        ax.annotate(txt, (x[i], y[i]))
    hull = ConvexHull(points)
    plt.plot(points[:,0], points[:,1], 'o')
    for simplex in hull.simplices:
        plt.plot(points[simplex, 0], points[simplex, 1], 'r-')
        
def normalize(s,up):
    n = len(s)
    difference = 1 - up
    S = np.zeros(n)
    for i in range(n):
        S[i] = s[i] + difference
    return S

def simulate_one_jump(proba):
    n = np.shape(proba)
    next_state = 0
    seed = random.uniform(0,1)
    while seed > sum(proba[0:(next_state+1)]):
                     next_state += 1
    return next_state    

def action_W(state,M):
    i,a = actives2(state,M)
    n = len(state)
    action = [0]*n
    for j in range(i):
        action[j] = state[j]
    action[i] = a
    return action

def one_transform(s,M,P0,P1):
    """
    We assume that P0 and P1 are already sorted by indices
    
    """
    i,a = actives2(s,M)
    n = np.shape(s)[0]
    new_s = np.zeros(n, dtype = int)
    for j in range(i):
        for iterate in range(s[j]):
            k = simulate_one_jump(P1[j])
            new_s[k] += 1
    for j in range(i+1,n):
        for iterate in range(s[j]):
            k = simulate_one_jump(P0[j])
            new_s[k] += 1 
    for iterate in range(a):
        k = simulate_one_jump(P1[i])
        new_s[k] += 1
    for iterate in range(s[i] - a):
        k = simulate_one_jump(P0[i])
        new_s[k] += 1
    return new_s

def one_simulation(M,s,P0,P1,R1,time_horizon = 1000):
    '''
    We assume that the paras are indexable and already sorted by indices
    
    '''
    N = np.sum(s)
    total_reward = 0.
    
    for i in range(time_horizon):
        s = one_transform(s,M,P0,P1)
        total_reward += np.dot(action_W(s,M),R1)
        
    return s,total_reward/(time_horizon*N)

def give_matrix(i,P0,P1):
    '''
    The program will return the transformation matrix when the threshold happens at state i
    
    '''
    P = np.copy(P0)
    for j in range(i):
        P[j] = P1[j] - P1[i] + P0[i]
    return P

def test_cycles(P0,P1,R1):
    n = len(R1)
    for j in range(n):
        P = give_matrix(j,P0,P1)
        eigens = np.linalg.eig(P)[0]
        norme = [abs(eigens[i]) for i in range(len(eigens))]
        norme.sort(reverse=True)
        if norme[0] > 1.001:
            return True
    return False 

def L2(s1,s2):
    d = 0.
    for i in range(len(s1)):
        d += (s1[i]-s2[i])**2
    return np.sqrt(d)

def upper_reward(m,alpha,R1):
    
    j = actives1(m,alpha)[0]
    somme = np.sum(m[0:j])
    upper = 0.
    for p in range(j):
        upper += R1[p]*m[p]
    upper += R1[j]*(alpha - somme)
    
    return upper